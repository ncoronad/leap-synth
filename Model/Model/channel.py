"""
An audio channel is a single processing stream that deals with one unit of
sound production, such as one oscillator.  When a note is pressed, a channel
is opened.  When a note is released, a channel is closed.
"""

from audio_buffer import *

class Channel:

    def __init__(self, id):
        self.id = id
        self.offset = 0
        self.release = {'releasing': False, 'offset': 0}
        self.note = None
        self.scheduled_note = None
        self.__callbacks = []

    def start_note(self, note):
        if (self.note):
            self.release_note()
            self.scheduled_note = note
        else:
            self.note = note

    def release_note(self):
        if (self.note):
            self.release['releasing'] = True

    def register_callback(self, callback):
        self.__callbacks.append(callback)

    def unregister_callback(self, callback):
        self.__callbacks.remove(callback)

    def write_to_buffer(self, buff):
        if (not self.note or len(self.__callbacks) == 0):
            return

        new_buffer = AudioBuffer(buff.size, self.offset)

        for callback in self.__callbacks:
            callback(new_buffer, self.note, self.release)

        # the note has fully released
        if (self.release['releasing'] and new_buffer.get_max() == 0):
            self.note = self.scheduled_note or None
            self.release = {'releasing': False, 'offset': 0}
            self.offset = 0
        elif (self.release['releasing']):
            self.release['offset'] += buff.size

        self.offset += buff.size
        buff.write_data(new_buffer.get())


