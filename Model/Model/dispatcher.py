
from channel import *

class ChannelDispatcher:

    def __init__(self, num_channels):
        self.channels = [Channel(x) for x in xrange(num_channels)]
        self.method_map = {
            'on': self.note_on,
            'off': self.note_off
        }

    def setup_callbacks(self, oscillators, engine):
        for channel in self.channels:
            for osc in oscillators:
                channel.register_callback(osc.write_to_buffer)

            engine.register_callback(channel.write_to_buffer)

    def kill_all(self):
        for channel in self.channels:
            channel.release_note()

    def note_event(self, event_type, note):
        event_callback = self.method_map[event_type]
        event_callback(note)

    def note_on(self, note):
        channel = self.get_channel_playing_note(note) or self.get_next_channel()
        channel.start_note(note)

    def note_off(self, note):
        channel = self.get_channel_playing_note(note)
        if (not channel):
            return

        channel.release_note()

    def get_channel_playing_note(self, note):
        for channel in self.channels:
            if (channel.note == note):
                return channel

        return False

    def get_next_channel(self):
        for channel in self.channels:
            if (not (channel.note or channel.scheduled_note)):
                return channel

        channel = self.channels[0]
        self.channels.append(self.channels.pop(0))
        return channel
