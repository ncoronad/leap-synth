##!/usr/bin/python

from audio_utils import get_freq_from_midi
from adsr import *

NUM_PARTIALS = 16
SAMPLE_RATE = 44100

class Oscillator:
    
    def __init__(self):
        self.active = False
        self.amplitude = 0.6
        self.partials = [Partial(x + 1) for x in xrange(NUM_PARTIALS)]
        self.adsr = ADSR(SAMPLE_RATE)

    def toggle(self):
        self.active = not self.active

    def write_to_buffer(self, buff, note, release):
        if (not self.active):
            return

        frequency = get_freq_from_midi(note)
        for partial in self.partials:
            partial.write_to_buffer(buff, frequency, self.amplitude)

        self.adsr.write(buff, release)

    # Overriding square brackets on self to fetch attribute by key
    def __getitem__(self, key):
        return getattr(self, key)


class Partial:

    def __init__(self, index, amplitude=0):
        # The index is the multiple of the fundamental frequency of a given note
        self.index = index
        self.amplitude = amplitude

    def set_amplitude(self, amplitude):
        amplitude = max(0, min(1, amplitude))
        self.amplitude = amplitude

    def write_to_buffer(self, buff, frequency, osc_amplitude):
        if (self.amplitude < 0.05):
            return

        amplitude = self.amplitude * osc_amplitude
        frequency *= self.index
        buff.write_sine(frequency, amplitude)
        return

    def __getitem__(self, key):
        return getattr(self, key)

