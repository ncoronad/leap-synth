#!/usr/bin/python

import pyaudio
import time

from audio_buffer import *

SAMPLE_RATE = 44100

class Engine:
    
    def __init__(self, buffer_size):
        self.stream = self.__init_stream(buffer_size)
        self.__callbacks = []

    def start_stream(self):
        self.stream.start_stream()
        #while self.stream.is_active():
        #    time.sleep(0.1)

    def stop_stream(self):
        self.stream.stop_stream()
        self.stream.close()

    def register_callback(self, callback):
        self.__callbacks.append(callback)

    def unregister_callback(self, callback):
        self.__callbacks.remove(callback)

    def __init_stream(self, buffer_size):
        p = pyaudio.PyAudio()
        return p.open(
            format=pyaudio.paFloat32,
            channels=1,
            rate=SAMPLE_RATE,
            output=True,
            frames_per_buffer=buffer_size,
            stream_callback=self.__audio_callback
        )

    def __audio_callback(self, in_data, frame_count, time_info, status):
        buff = AudioBuffer(frame_count)
        for callback in self.__callbacks:
            callback(buff)

        data = buff.get()
        # TODO: figure out how to auto scale the output based on the
        # number of channels and partials involved so that it doesn't peak such
        # that the user can't tell it's happening
        data = data / 100.0
        return (data, pyaudio.paContinue)



