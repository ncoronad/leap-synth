
from scipy import interpolate
import numpy as np

class ADSR:

    def __init__(self, rate):
        # TODO: move to configs
        self.attack = 0.1
        self.decay = 0.05
        self.sustain = 1.0
        self.release = 0.01
        self.sample_rate = rate
        self.generate_attack()
        self.generate_release()

    def __getitem__(self, key):
        return getattr(self, key)

    def write(self, buff, release):
        if (buff.offset < self.attack_buffer.size):
            self.write_attack(buff)
        elif (release['releasing']):
            self.write_release(buff, release)
        else:
            buff.multiply(self.sustain)

    def write_attack(self, buff):
        start = buff.offset
        end = start + buff.size
        
        data = self.attack_buffer[start:end]
        if (data.size < buff.size):
            fill_size = buff.size - data.size
            data = self.fill_buff(data, fill_size, self.sustain)
        buff.multiply(data)

    def write_release(self, buff, release):
        start = release['offset']
        if (start >= self.release_buffer.size):
            return buff.multiply(0)

        end = start + buff.size
        data = self.release_buffer[start:end]
        if (data.size < buff.size):
            fill_size = buff.size - data.size
            data = self.fill_buff(data, fill_size, 0.0)
        buff.multiply(data)

    def generate_attack(self):
        total_attack_time = self.attack + self.decay
        total_attack_samps = total_attack_time * self.sample_rate
        peak = self.attack / total_attack_time
        keys = [0.0, peak, 1.0]
        vals = [0.0, 1.0, self.sustain]
        interp = interpolate.interp1d(keys, vals, kind='slinear')
        factor = 1.0 / total_attack_samps
        attack_array = np.arange(total_attack_samps, dtype=np.float32)
        self.attack_buffer = interp(attack_array * factor)

    def generate_release(self):
        total_decay_samps = round(self.sample_rate * self.decay)
        keys = [0.0, 1.0]
        vals = [self.sustain, 0.0]
        interp = interpolate.interp1d(keys, vals, kind='slinear')
        factor = 1.0 / total_decay_samps
        self.release_buffer = interp(np.arange(total_decay_samps) * factor)

    def fill_buff(self, buff, fill_size, fill_value):
        fill = np.ones(fill_size, dtype=np.float32) * fill_value
        return np.concatenate([buff, fill])
        


