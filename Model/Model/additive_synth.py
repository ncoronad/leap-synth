#!/usr/bin/python

import json

from engine import *
from channel import *
from oscillator import *
from dispatcher import *

# TODO create a config to store and retrieve these macros
MAX_CHANNELS = 8
NUM_OSCILLATORS = 4
BUFFER_SIZE = 2048

# Eventually this should inherit from a Synth super class. This'll work for now
class AdditiveSynth:

    def __init__(self):
        self.oscillators = [Oscillator() for x in xrange(NUM_OSCILLATORS)]
        self.dispatcher = ChannelDispatcher(MAX_CHANNELS)
        self.load_data()
        self.engine = Engine(BUFFER_SIZE)
        self.dispatcher.setup_callbacks(self.oscillators, self.engine)
        self.engine.start_stream()

    def load_data(self):
        with open('presets.json') as data_file:
            presets = json.load(data_file)
        last_saved = presets['last_saved']
        preset = presets[last_saved] if last_saved else presets['default']
        # TODO: create update methods for all classes that handle changes well
        self.update(preset)

    def note_event(self, event_type, note):
        self.dispatcher.note_event(event_type, note)

    def kill_all(self):
        self.dispatcher.kill_all()

    # TODO: better error handling, validate input and bind to ranges
    # perhaps each object should have its own update function to handle its
    # own validation and error handling logic
    def update(self, data, obj=None):
        obj = obj or self
        for key, val in data.items():
            if (isinstance(val, list)):
                for i in xrange(0, len(val)):
                    self.update(val[i], obj[key][i])
            elif (isinstance(val, dict)):
                self.update(val, obj[key])
            else:
                setattr(obj, key, val)

    # Override square brackets on self to get attribute by key
    def __getitem__(self, key):
        return getattr(self, key)

