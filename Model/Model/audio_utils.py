# Audio utils module.  Some util functions for common music math

import numpy as np

# Constant used to make the equal temperment scale work
FREQ_CONST = 16.35159783128741

def get_freq_from_midi(midi_num):
    octave = midi_num / 12
    note = midi_num % 12
    base_freq = FREQ_CONST * 2.0 ** (float(note) / 12.0)
    return base_freq * (2.0 ** octave)

def create_sine(frequency, amplitude, length, offset, rate):
    factor = float(frequency) * (np.pi * 2) / rate
    return np.sin(np.arange(offset, offset + length, dtype=np.float32) * factor)

