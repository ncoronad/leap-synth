"""
This module is for creating, writing and reading audio buffers.
"""

import numpy as np

from audio_utils import *

SAMPLE_RATE = 44100

class AudioBuffer:

    def __init__(self, size, offset=0):
        self.size = size
        self.offset = offset
        self.__data = np.zeros(self.size, dtype=np.float32)

    def get(self):
        return (self.__data).astype(np.float32)

    def write_data(self, data):
        if (not len(data) or data.size != self.size):
            return
        self.__write(data)

    def multiply(self, data):
        #if (data.size and data.size != self.__data.size):
        #    raise BufferError("cannot multiply buffers of different sizes")
        self.__data = self.__data * data

    def write_sine(self, freq, amp):
        data = create_sine(freq, amp, self.size, self.offset, SAMPLE_RATE)
        self.__write(data)

    def get_max(self):
        return np.amax(self.__data)
    
    def __write(self, data):
        self.__data = self.__data + data
 
